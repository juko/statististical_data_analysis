[![Binder](https://binderdemo.mpcdf.mpg.de/badge_logo.svg)](https://binderdemo.mpcdf.mpg.de/v2/glMPCDF/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fjuko%2Fstatististical_data_analysis.git/master)

# Practical Course "Modern Statistical Data Analysis for Practitioners"

## Crash course to Python and Jupyter notebooks
## Visualizing Data 
## Probability distributions 
## P-values as a measure of surprise
## Maximum likelihood 
## Bayesian inference
