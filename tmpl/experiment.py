"""
Simulates a measurement 

Functions: 

x,y = do_the_experiment()
"""
import numpy as np
import scipy
    
def do_the_experiment(Ns=10, loc=10, scale=0.5, fac=10):
    """
    Parameters
    ----------
    Ns: int
        Number of samples
    loc: float
        Location parameter
    scale: float
        Scale parameter
    fac: float
        Determines the detector size, i.e., the range of the histogram in 'fac' multiples 
        of the 'scale' from 'loc'
        
    Returns
    -------
    x: numpy array
        Independent variable
    y: numpy array 
        Data valus
    """
    bin_fac=2
    xmin=loc-fac*scale
    xmax=loc+fac*scale
    p_min=scipy.stats.cauchy.cdf(xmin, loc=loc, scale=scale)
    p_max=scipy.stats.cauchy.cdf(xmax, loc=loc, scale=scale)

    p_uniform=np.random.uniform(p_min,p_max,Ns)
    x_cdf=scipy.stats.cauchy.ppf(p_uniform, loc=loc, scale=scale)

    ind=np.where((x_cdf>xmin) &  (x_cdf<xmax))
    x_cdf=x_cdf[ind]
    h, e = np.histogram(x_cdf, range=(xmin, xmax), bins=fac*2*bin_fac)
    c=(e[1:]+e[:-1])/2.
    return c, h
